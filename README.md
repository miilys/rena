# rena

A (simple?) renaming utility.

## Requirements

- clap
- color-eyre
- paris
- rayon
- strfmt
- regex (soon)

## Usage

See `--help` for now, full documentation to come.
